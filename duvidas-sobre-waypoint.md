Não confundir o WP com ferramenta de CI/CD!

0. O que é Waypoint?

A implantação de aplicativos no cenário DevOps pode ser confusa com tantos serviços, arquivos de configuração e fluxos de trabalho para decodificar. Como desenvolvedor, você deseja apenas implantar seu serviço. O Waypoint pode ajudar com isso.

O Waypoint permite *publicar qualquer aplicativo em qualquer plataforma com um único arquivo e um único comando*: waypoint up. Esta página descreve como o Waypoint funciona. Quando estiver pronto para experimentar o Waypoint, siga os próximos tutoriais para instalar, configurar e executar o Waypoint.

O Waypoint funciona com JavaScript, Python, PHP, Java, Ruby e qualquer outra linguagem que você possa criar com Cloud Native Buildpacks .

1. Eduardo, onde encontrar informações sobre o Waypoint?

Você pode visitar a página oficial do projeto que fica em https://www.waypointproject.io e ainda ter acesso às discussões https://discuss.hashicorp.com/c/waypoint/51.

2. Achei meio complicado... o gitlab-ci parece mais funcional visto que no repositório ja vai ter o controle de versão, registry etc, eu teria que integrar o waypoint com o gitlab se eu quisesse usar os 2 certo? Parece que daria mais trabalho, ou to viajando? Mas pra um dev que quer testar o ci/cd local é bem válido.

R: GitLab é uma ferramenta de CI/CD. Não realiza necessariamente a abstração promovida pelo WP.
Quantas linhas de yaml foram escritas usando o WP para deployar uma app no kubernetes?

3. Como tu vê o Waypoint concorrendo com o Tekton ? o Waypoint me pareceu muito mais sucinto nas configuraćões, ao passo que o Tekton, por estar embedded no próprio cluster K8s, parece-me mais poderoso. Qual a tua opinião sobre isso?

R: Como vc disse: O Tekton é uma ferramenta de automação de fluxo de trabalho (workflow) e CI/CD (Integração Contínua/Entrega Contínua) baseada em Kubernetes. No entanto, o objetivo do WP é criar a abstração para entrega seja lá no k8s, Docker, Nomad ou no seu provider de preferência.

4. fiquei com dúvidas, qual a vantagem desse Waypoint quando comparado com as ferramentas de CI/CD já existentes?

R: direto da sua máquina, conseguirá buildar e fazer o deploy em qualquer lugar, somente com um comando.

5. "Obrigado pelo vídeo, não vejo um grande ou grande benefício em usar isso. A maioria dos desenvolvedores não configura CI/CD, também existem muitas ferramentas que fornecem controle via interface do usuário para pipelines como jenkins e facilitam muito o manuseio de pipelines. Uma característica importante que vejo é a capacidade de trabalhar com chatops, além disso, é como tentar encaixar um elefante em uma geladeira"

R: O principal objetivo deste projeto é abordar a parte CD do CI/CD. Portanto, não importa onde um desenvolvedor implanta, existe um fluxo de trabalho comum. Em última análise, será a comunidade que determinará a utilidade desse projeto. Vamos esperar para ver...

R2: Eu acho que há outra grande vantagem ... Pelo vídeo parece muito leve e requer tão pouco esforço para configurar que comecei a pensar em usá-lo para projetos menores onde não tenho disposição para configurar jenkins ou qualquer coisa. 

Outra grande vantagem: parece ser compatível com várias nuvens, o que torna a parte CI/CD também portátil, o que é uma grande ajuda se você quiser mover seu produto de uma nuvem para outra. E você não só é capaz de integrá-lo apenas com a plataforma de nuvem, mas também com outras soluções de CI/CD existentes, o que significa que sua configuração hcl pode atuar como uma linguagem padrão universal para ci/cd que pode ser interpretada pela maioria dos populares atualmente tecnologias que permitem que você mova rapidamente suas tarefas ci/cd entre diferentes tecnologias sem refatorá-las.

Construído para facilitar a implantação contínua para desenvolvedores.

6. Quando o waypoint é implantado, é uma espécie de implantação verde azul. Ele cria uma nova implantação e depois altera a tag no serviço.
Eu queria saber se há mais alguma coisa para implementar para ter uma implantação verde azul perfeita.

E como você exclui automaticamente a implantação antiga no kubernetes para ter um ci/cd completo?

R: O Waypoint não pretende ser um sistema CI/CD, mas pode ser integrado a um.

A melhor opção para automatizar a execução é usar a integração Git nativa do Waypoint. Isso observará um repositório Git e executará um waypoint up sempre que uma alteração for detectada. Esta é a abordagem recomendada.

https://developer.hashicorp.com/waypoint/docs/automating-execution
