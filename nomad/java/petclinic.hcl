job "petclinic" {

  group "spring" {
    network {
      port "http" {
        to = 8080
      }
    }

    task "petclinic" {
      driver = "docker"

      config {
        image = "petclinic:v1"
        ports = ["http"]
      }

      resources {
        cpu    = 500
        memory = 1024
      }
    }
  }
}
