job "redis-podman" {

  group "cache" {
    network {
      port "db" {
        to = 6379
      }
    } 

    service {
      name     = "redis-cache"
      tags     = ["global", "cache"]
      port     = "db"
      provider = "nomad"

      check {
        name     = "alive"
        type     = "tcp"
        interval = "10s"
        timeout  = "2s"
      }
    }

    task "redis" {
      driver = "podman"

      config {
        image          = "docker://redis:7"
        ports          = ["db"]
      }

      identity {
        env  = true
        file = true
      }

      resources {
        cpu    = 500
        memory = 256
      }
    }
  }
}
