job "mysql-volume" {
  datacenters = ["dc1"]

  group "database" {

    # Registrar no stanza client no /etc/nomad.d/nomad.hcl
    volume "mysql" {
      type      = "host"
      read_only = false
      source    = "mysql"
    }

    network {
      port "db" {
        static = 3306
      }
    }

    task "mysql" {
      volume_mount {
        volume      = "mysql"
        destination = "/var/lib/mysql"
        read_only   = false
      }

      env = {
        MYSQL_DATABASE      = "db"
        MYSQL_USER          = "user"
        MYSQL_PASSWORD      = "password"
        MYSQL_ROOT_PASSWORD = "password"
      }

      driver = "docker"
      config {
        image = "mysql:5.7"
        ports = ["db"]
      }
      
      resources {
        cpu    = 500
        memory = 1024
      }

      service {
        name     = "mysql"
        provider = "nomad"
        port     = "db"
        check {
          type     = "tcp"
          interval = "3s"
          timeout  = "1s"
        }
      }
    }
  }
}

