job "mysql-env" {
  datacenters = ["dc1"]

  group "database" {
    network {
      port "db" {
        to = 3306
        static = 3306
      }
    }

    task "mysql" {
      template {
        destination = "local/mysql.env"
        env         = true
        data        = <<EOF
    {{- with nomadVar "nomad/jobs/database" -}}
      MYSQL_DATABASE = "db"
      MYSQL_USER = "{{.user}}"
      MYSQL_PASSWORD = "{{.password}}"
      MYSQL_ROOT_PASSWORD = "{{.password}}"
    {{- end -}}
    EOF
  }
      driver = "docker"
      config {
        image  = "mysql:5.7"
        ports = ["db"]
        volumes = [
          "/opt/mysql/data:/var/lib/mysql",
        ]
      }
    }
  }
}

