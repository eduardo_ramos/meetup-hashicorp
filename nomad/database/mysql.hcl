job "mysql-server" {
  datacenters = ["dc1"]

  group "database" {
    network {
      port "db" {
        to = 3306
        static = 3306
      }
    }

    task "mysql" {

    env = {
      MYSQL_DATABASE = "db"
      MYSQL_USER = "user"
      MYSQL_PASSWORD = "password"
      MYSQL_ROOT_PASSWORD = "password"
    }

      driver = "docker"
      
      config {
        image  = "mysql:5.7"
        ports = ["db"]
        volumes = [
          "/opt/mysql/data:/var/lib/mysql",
        ]
      }
      
      resources {
        cpu    = 200
        memory = 256
      }

      service {
        name = "mysql"
        provider = "nomad"
        port = "db"
        check {
          type     = "tcp"
          interval = "3s"
          timeout  = "1s"
        }

      }
    }
  }
}

