job "gitlab-runner" {
  datacenters = ["dc1"]

  group "gitlab" {
    count = 1

    service {
      name = "runner"
      provider = "nomad"
    }

    task "runner" {
      driver = "docker"

      config {
        image = "gitlab/gitlab-runner:latest"
        volumes = [
          "/var/run/docker.sock:/var/run/docker.sock",
          "local/config.toml:/etc/gitlab-runner/config.toml",
        ]
      }

      template {
        data          = <<EOF
concurrent = 1
check_interval = 0
shutdown_timeout = 0

[session_server]
  session_timeout = 1800

[[runners]]
  name = "debian"
  url = "https://gitlab.com/"
  id = 21632875
  token = "v8uU_yizoPmgCez7MRcZ"
  token_obtained_at = 2023-03-07T00:01:29Z
  token_expires_at = 0001-01-01T00:00:00Z
  executor = "docker"
  [runners.cache]
    MaxUploadedArchiveSize = 0
  [runners.docker]
    tls_verify = false
    image = "ubuntu:18.04"
    privileged = false
    disable_entrypoint_overwrite = false
    oom_kill_disable = false
    disable_cache = false
    volumes = ["/cache"]
    shm_size = 0
EOF
        destination   = "local/config.toml"
        change_mode   = "signal"
        change_signal = "SIGHUP"
      }
    }
  }
}
