

# PLEASE NOTE: If you copy this file, be sure to replace <YOUR HOSTNAME>
# Your hostname can be attained by running the "identity" command on your node.

job "lxc" {
  datacenters = ["dc1"]
  type        = "service"

  group "linux" {
    task "busybox" {
      driver = "lxc"

      config {
        log_level = "trace"
        verbosity = "verbose"
        template  = "/usr/share/lxc/templates/lxc-busybox"
       }
      resources {
        cpu    = 1000
        memory = 1024
      }
    }
  }
}
