job "qemu" {
  datacenters = ["dc1"]

  group "tinycore" {
    network {
      mode = "bridge"
      port "http" {
        to = 80
      }
      port "ssh" {
        to = 2022
      }
    }

    task "tc" {
      template {
        data = <<EOH
      Guest System
      EOH

        destination = "local/index.html"
      }

      artifact {
        source      = "https://github.com/eduardosramos/nomad_example_jobs/raw/main/qemu/tinycore.qcow2"
        destination = "local"
      }

      service {
        tags     = ["tag1"]
        port     = "http"
        provider = "nomad"

        check {
          type     = "http"
          port     = "http"
          path     = "/index.html"
          interval = "10s"
          timeout  = "2s"
        }
      }

      driver = "qemu"
      config {
        image_path  = "local/tinycore.qcow2"
        accelerator = "kvm"
        args = [
          "-device",
          "e1000,netdev=user.0",
          "-netdev",
          "user,id=user.0,hostfwd=tcp::${NOMAD_PORT_http}-:80,hostfwd=tcp::${NOMAD_PORT_ssh}-:22",
        ]
      }
      resources {
        cores  = 1
        memory = 1024
      }
    }
  }
}

