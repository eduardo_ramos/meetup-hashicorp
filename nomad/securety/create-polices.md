https://developer.hashicorp.com/nomad/tutorials/access-control/access-control-create-policy

[root@waypoint polices]# nomad acl token create -name="Test app-dev token" -policy=app-dev -type=client | tee app-dev.token
Accessor ID  = ab296aff-19f1-55e4-8aa8-3b2c0afaeda4
Secret ID    = a774f1f9-5f24-712d-b5be-cc4bf7deb9a6
Name         = Test app-dev token
Type         = client
Global       = false
Create Time  = 2023-06-15 12:39:13.552840935 +0000 UTC
Expiry Time  = <none>
Create Index = 788
Modify Index = 788
Policies     = [app-dev]

Roles
<none>

[root@waypoint polices]# nomad acl token create -name="Test prod-ops token" -policy=prod-ops -type=client | tee prod-ops.token
Accessor ID  = 9b538502-93ee-5266-4d93-4f5a4b0f6e0d
Secret ID    = b2e0baf7-b538-364c-1be6-b3a69952b119
Name         = Test prod-ops token
Type         = client
Global       = false
Create Time  = 2023-06-15 12:41:04.950469856 +0000 UTC
Expiry Time  = <none>
Create Index = 792
Modify Index = 792
Policies     = [prod-ops]

Roles
<none>


