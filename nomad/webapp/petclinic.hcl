job "petclinic" {
  datacenters = ["dc1"]

  group "petclinic" {
    count = 1

    update {
      max_parallel     = 1
      canary           = 1
      min_healthy_time = "30s"
      healthy_deadline = "5m"
      auto_revert      = false
      auto_promote     = false
    }

    network {
      port "http" {
        to = 8080
      }
    }

    service {
      name        = "petclinic"
      port        = "http"
      provider    = "nomad"
      tags        = ["meetup"]
      canary_tags = ["canary"]

      check {
        type     = "http"
        port     = "http"
        path     = "/"
        interval = "5s"
        timeout  = "2s"
      }
    }

    task "petclinic" {
      driver = "docker"

      config {
        image = "laoqui/spring-petclinic:v2.0"
        ports = ["http"]
      }

      resources {
        cores    = 1
        memory = 1000
      }
    }
  }
}
