job "batch-cron" {
  datacenters = ["dc1"]

  type = "batch"
  periodic {
    cron  = "* * * * *"
    prohibit_overlap = true
    time_zone = "America/Sao_Paulo"
  }

  group "echo" {
    count = 1
    constraint {
      distinct_hosts = true
    }
    task "echo-date" {
      driver = "raw_exec"
      config {
        command = "/bin/bash"
        args    = ["-c", "echo $(date) >> /tmp/echo.txt"]
      }
    }
  }
}

