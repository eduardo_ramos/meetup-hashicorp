## Como instalar o Waypoint:

1. Preparando para a instalação.

 - Criar os volumes no servidor do Nomad:

OBS: Necessário criar antes os diretórios.

```bash
$ mkdir -p /nomad/{host-volumes/wp-server,host-volumes/wp-runner}

```
OBS: Exemplo de arquivo que sobre client/servidor na mesma máquina. Não recomendado para ambientes produtivos.[1]

 - Configurar os volumes no /etc/nomad.d/nomad.hcl

```hcl
data_dir  = "/opt/nomad/data"
bind_addr = "127.0.0.1"

advertise {
  http = "{{ GetInterfaceIP \"eth0\" }}"
  rpc  = "{{ GetInterfaceIP \"eth0\" }}"
  serf = "{{ GetInterfaceIP \"eth0\" }}"
}

server {
  enabled          = true
  bootstrap_expect = 1
}

client {
  enabled = true
  servers = ["127.0.0.1"]

  host_volume {
    wp-server-vol {
      path = "/nomad/host-volumes/wp-server"
      read_only = false
    }

    wp-runner-vol {
      path = "/nomad/host-volumes/wp-runner"
      read_only = false
    }
  }

  options = {
      "driver.raw_exec.enable" = "true"
      "docker.privileged.enabled" = "true"
      "docker.volumes.enabled" = "true"
  }
}

plugin "docker" {
   config {
    auth {
      config = "/home/eduardo/.docker/config.json"
    }
    allow_privileged = true
    volumes {
      enabled      = true
    }
  }
}

```
Restart o Nomad.

```bash
$ systemctl restart nomad
```

2. Instalando o Waypoint

O passo de instalação abaixo disponibilizará dois jobs no Nomad: Um servidor do Waypoint e um Waypoint Runner. Em seguida os demais parâmetros dizem respeito a aceitação dos termos de serviço, cria os arquivos de persistência de dados em seus respectivos volumes previamente criados e não faz o registro do serviço no Consul.


```bash
$ waypoint install -platform=nomad -accept-tos -nomad-host-volume=wp-server-vol -nomad-runner-host-volume=wp-runner-vol -nomad-consul-service=false

✓ Waypoint server ready
Waypoint server running on Nomad is being accessed via its allocation IP and port.
This could change in the future if Nomad creates a new allocation for the Waypoint server,
which would break all existing Waypoint contexts.

It is recommended to use Consul for determining Waypoint servers IP running on Nomad rather than
relying on the static IP that is initially set up for this allocation.
✓ Configured server connection
✓ Successfully connected to Waypoint server in Nomad!
✓ Server installed and configured!
✓ Runner "static" installed
✓ Registered ondemand runner!
✓ Initializing Nomad client...
✓ Waypoint runner installed
✓ Runner "static" adopted successfully.
Waypoint server successfully installed and configured!

The CLI has been configured to connect to the server automatically. This
connection information is saved in the CLI context named "install-1686581538".
Use the "waypoint context" CLI to manage CLI contexts.

The server has been configured to advertise the following address for
entrypoint communications. This must be a reachable address for all your
deployments. If this is incorrect, manually set it using the CLI command
"waypoint server config-set".

To launch and authenticate into the Web UI, run:
waypoint ui -authenticate

Advertise Address: 192.168.1.15:9701
Web UI Address: https://192.168.1.15:9702
```

É possível acessar diretamente pelo "waypoint ui -authenticate" ou ir até a página e solicitar a criação de um token:

```bash
sudo waypoint user token
```
Neste ponto, será possível acessar a página central de projetos do Waypoint.

![plot](./images/authenticate-waypoint.png)

### Criando nosso primeiro projeto
![plot](./images/projects.png)

Vamos acessar a pasta labs e subir nosso primeiro projeto. Vamos começar usando um exemplo em Docker.

```bash
$ cd labs/docker
$ 

```

Fontes:  
[1] Disponível em: <https://discuss.hashicorp.com/t/is-there-a-low-resource-configuration-or-distribution-of-nomad-like-k3s-or-can-it-be-run-on-a-single-weaker-machine/24081/2> Acesso em: 12 jun 2023.  

[2] Disponível em:<https://developer.hashicorp.com/waypoint/docs/projects/git?ajs_aid=964e05e4-3ee9-407d-9677-9a22d8ca4789&_gl=1*p9sk9q*_ga*NTM4Nzc4MTQ5LjE2NzI2NzkwOTA.*_ga_P7S46ZYEKW*MTY4NTIyOTQxNi40Mi4xLjE2ODUyMjk2OTYuNi4wLjA.&product_intent=waypoint> Acesso em: 12 jun 2023.  

[3] Disponível em: <> Acesso em: 



